#ifndef __buildinflash_H__
#define __buildinflash_H__	
#include "stm32f10x.h"
#include "stm32f10x_flash.h" 

///* 类型定义 ------------------------------------------------------------------*/
typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

/* 宏定义 --------------------------------------------------------------------*/
#define FLASH_PAGE_SIZE           ((uint16_t)0x400)        //页大小   - 2K
#define FLASH_TYPE_LENGTH         ((uint16_t)0x002)        //类型大小 - 2字节
#define FLASH_PAGE_LENGTH         (FLASH_PAGE_SIZE/FLASH_TYPE_LENGTH)
#define FLAG_OK                   0x00
#define FLAG_NOOK                 0x01

///* 函数申明 ------------------------------------------------------------------*/
void FLASH_PageWrite(uint16_t *pBuffer, uint32_t WriteAddr);
void FLASH_WriteNWord(uint16_t* pBuffer, uint32_t WriteAddr, uint16_t nWord);
void FLASH_ReadNWord(uint16_t* pBuffer, uint32_t ReadAddr, uint16_t nWord);
//void ReadFlashNBtye(uint32_t ReadAddress, uint8_t *ReadBuf, int32_t ReadNum);


#endif  /* __buildinflash_H__ */
