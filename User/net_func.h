#ifndef  __NET_FUNC_H
#define	 __NET_FUNC_H

#include "stm32f10x.h"

/**********************************参数********************************/
#define      macUser_ESP8266_ApSsid                       "OpenWrt"                //要连接的热点的名称
#define      macUser_ESP8266_ApPwd                        "963.8520"           //要连接的热点的密钥

#define      macUser_ESP8266_TcpServer_IP                 "192.168.1.157"      //要连接的服务器的 IP
#define      macUser_ESP8266_TcpServer_Port               "8888"               //要连接的服务器的端口

extern volatile uint8_t ucTcpClosedFlag;

void ESP8266_SAT_mode(void);
void Start_TCP_Client(void);
#endif


