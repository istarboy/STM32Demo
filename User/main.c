#include "stm32f10x.h"
#include "stdlib.h"
#include "bsp/usart/bsp_debug_usart.h"
#include "bsp/RC522/rc522_function.h"
#include "bsp/RC522/rc522_config.h"
#include "bsp/systick/bsp_SysTick.h"
#include "bsp/NRF24l01/NRF24l01.h"
#include "bsp/buildinflash/buildinflash.h"
unsigned char admin_card[4]={0x94,0xA7,0x98,0x12};
unsigned char empty_data[4]={0xFF,0xFF,0xFF,0xFF};
#define FLASH_ADDR_FLAG           0x08005000     //FLASH地址 - 标志位
uint8_t WriteBuf[4];    
uint8_t ReadBuf[50];
uint8_t rece_buf[32];
uint8_t send_buf[32];
char cStr [ 30 ];
unsigned char ucArray_ID [ 4 ];
uint16_t flag_data,clear_data,last_flag=0; 
uint32_t FLASH_ADDR_DATA=0x08005002;
void Switch_Beep_Init(void)
{
 
 GPIO_InitTypeDef  GPIO_InitStructure;
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
// GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;				 
// GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 
// GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 
// GPIO_Init(GPIOA, &GPIO_InitStructure);					 
// GPIO_SetBits( GPIOA, GPIO_Pin_2 );						 
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;				 
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 
 GPIO_Init(GPIOA, &GPIO_InitStructure);					 
 GPIO_SetBits( GPIOA, GPIO_Pin_3 );		

}

void InitSys(void)
{
	SystemInit();
  DEBUG_USART_Init(); 
  SysTick_Init();  
	RC522_Init (); 
	NRF24L01_Init();
//	ESP8266_Init ();  
	Switch_Beep_Init();
	printf("Start");
	Delay_ms(100);
  printf("Init system\r\n");  
	if(NRF24L01_Check())
	{
		printf("24L01 Not Found\r\n");
	}else{
		printf("24L01 Checked\r\n");
		NRF24L01_TX_Mode();
	}
	PcdReset();
	printf("rc522 init susccessful\r\n");
	M500PcdConfigISOType ( 'A' );//设置工作方式
	FLASH_ReadNWord(&flag_data, FLASH_ADDR_FLAG, 1);
	NRF24L01_TX_Mode();
	Delay_ms(500);
	send_buf[0]=1;
	send_buf[1]=flag_data;
	if(NRF24L01_TxPacket(send_buf)==TX_OK)
	{
		printf("Successful Send data\r\n");
	}else{
		printf("Send Data Failed\r\n");
	}
	sprintf(cStr,"Count is %02X ",flag_data);
	printf ( "%s\r\n",cStr );
}

void BeepOn(int time)
{
		GPIO_ResetBits ( GPIOA, GPIO_Pin_3 );
		Delay_ms(time);
		GPIO_SetBits( GPIOA, GPIO_Pin_3 );
}

void WriteUser(void)
{
	FLASH_ReadNWord(&flag_data, FLASH_ADDR_FLAG, 1);
	WriteBuf[0]= ucArray_ID[0];WriteBuf[1]= ucArray_ID[1];WriteBuf[2]= ucArray_ID[2];WriteBuf[3]= ucArray_ID[3];
	FLASH_ADDR_DATA = 0x08005002+flag_data*4;
	FLASH_WriteNWord((uint16_t*)&WriteBuf, FLASH_ADDR_DATA, 2);
	flag_data = flag_data + 1;
	FLASH_WriteNWord(&flag_data, FLASH_ADDR_FLAG, 1);
	BeepOn(100);
	Delay_ms(200);
	BeepOn(100);
}

void ReadUser(void)
{
	int j;
	FLASH_ReadNWord(&flag_data, FLASH_ADDR_FLAG, 1);
	for (j=0;j<=flag_data;j++){
		FLASH_ReadNWord((uint16_t*)&ReadBuf, 0x08005002+j*4, 2);
		if((ReadBuf[0]==empty_data[0])&&(ReadBuf[1]==empty_data[1])&&(ReadBuf[2]==empty_data[2])&&(ReadBuf[3]==empty_data[3]))
		{
			//printf("ErroData");
		}else{
			if(flag_data!=last_flag){
				Delay_ms(500);
				send_buf[0]=4;
				send_buf[1]=ReadBuf[0];
				send_buf[2]=ReadBuf[1];
				send_buf[3]=ReadBuf[2];
				send_buf[4]=ReadBuf[3];
				if(NRF24L01_TxPacket(send_buf)==TX_OK)
				{
					printf("Successful Send data\r\n");
				}else{
					printf("Send Data Failed & Retry\r\n");
					Delay_ms(500);
					send_buf[0]=4;
					send_buf[1]=ReadBuf[0];
					send_buf[2]=ReadBuf[1];
					send_buf[3]=ReadBuf[2];
					send_buf[4]=ReadBuf[3];
					if(NRF24L01_TxPacket(send_buf)==TX_OK)
					{
						printf("Retry Send data Successful\r\n");
					}else{
						printf("Retry Faild LogData\r\n");
					}
				}
				sprintf(cStr,"Count is %02X ",flag_data);
				printf ( "%s\r\n",cStr );
				sprintf ( cStr, "Get Card ID is: %02X%02X%02X%02X", ReadBuf [ 0 ], ReadBuf [ 1 ], ReadBuf [ 2 ], ReadBuf [ 3 ] );							
				printf ( "%s\r\n",cStr );
				last_flag=0;
			}
		}
		Delay_ms(100);
	}
	last_flag=flag_data;
}

void CheckStatus(void)
{
	int j;
	FLASH_ReadNWord(&flag_data, FLASH_ADDR_FLAG, 1);
	for (j=0;j<=flag_data;j++){
		FLASH_ReadNWord((uint16_t*)&ReadBuf, 0x08005002+j*4, 2);
		if((ucArray_ID[0]==ReadBuf [ 0 ])&&(ucArray_ID[1]==ReadBuf [ 1 ])&&(ucArray_ID[2]==ReadBuf [ 2 ])&&(ucArray_ID[3]==ReadBuf [ 3 ]))
		{
//			DoorOpen();
			BeepOn(200);
			printf ( "Data Match\r\n");
			send_buf[0]=5;
			send_buf[1]=0xFF;
			send_buf[2]=0xFF;
			send_buf[3]=0xFF;
			send_buf[4]=0xFF;
			send_buf[5]=0xFF;
				if(NRF24L01_TxPacket(send_buf)==TX_OK)
				{
					printf("Successful Send data\r\n");
				}else{
					printf("Send Data Failed & Retry\r\n");
					Delay_ms(500);
					send_buf[0]=5;
					send_buf[1]=0xFF;
					send_buf[2]=0xFF;
					send_buf[3]=0xFF;
					send_buf[4]=0xFF;
					send_buf[5]=0xFF;
					if(NRF24L01_TxPacket(send_buf)==TX_OK)
					{
						printf("Retry Send data Successful\r\n");
					}else{
						printf("Retry Faild LogData\r\n");
					}
		}
		Delay_ms(100);
		//printf ( "\r\n");
	}
}}

void ClearFlashCount(void)
{
	//Flash清空操作 区域 FLASH_ADDR_DATA+500
	int i=0;
	printf("Clear Flash\r\n");
	clear_data=0xFFFF;
	for(i=0;i<500;i++)
	{
		FLASH_WriteNWord(&clear_data,FLASH_ADDR_DATA+i, 1);
	}
	printf("Clear Flash successful\r\n");
	flag_data=0;
	FLASH_WriteNWord(&flag_data, FLASH_ADDR_FLAG, 1);
	printf("count clear successful\r\n");
}

int main(void)
{    
	int flag;
	uint8_t ucStatusReturn;          //返回状态
	InitSys();
	//ClearFlashCount();
	BeepOn(100);
	Delay_ms(200);
	BeepOn(100);
//	ESP8266_SAT_mode();
  while ( 1 )
  {	
		FLASH_ReadNWord(&flag_data, FLASH_ADDR_FLAG, 1);
		//读取卡ID第一次
		if ( ( ucStatusReturn = PcdRequest ( PICC_REQALL, ucArray_ID ) ) != MI_OK )/*若失败再次寻卡*/
       ucStatusReturn = PcdRequest ( PICC_REQALL, ucArray_ID );		   
		if ( ucStatusReturn == MI_OK  )
		{
			/*防冲撞（当有多张卡进入读写器操作范围时，防冲突机制会从其中选择一张进行操作）*/
			if ( PcdAnticoll ( ucArray_ID ) == MI_OK )  
			{
				sprintf ( cStr, "The Card ID is: %02X%02X%02X%02X", ucArray_ID [ 0 ], ucArray_ID [ 1 ], ucArray_ID [ 2 ], ucArray_ID [ 3 ] );							
				printf ( "%s\r\n",cStr );
					//判断是否管理员卡
					if((ucArray_ID[0]==admin_card[0])&&(ucArray_ID[1]==admin_card[1])&&(ucArray_ID[2]==admin_card[2])&&(ucArray_ID[3]==admin_card[3])){
							printf("Auth Successful\r\n");
							Delay_ms(500);
							printf("Enter Add Mode\r\n");
							BeepOn(100);
							flag=1;
							//状态位置1进入管理模式
							while (flag==1){
									if ( ( ucStatusReturn = PcdRequest ( PICC_REQALL, ucArray_ID ) ) != MI_OK )/*若失败再次寻卡*/
											ucStatusReturn = PcdRequest ( PICC_REQALL, ucArray_ID );	
										//读取到卡ID
									if ( ucStatusReturn == MI_OK  ){
										//检测到新卡非管理ID写入Flash
											if (( PcdAnticoll ( ucArray_ID ) == MI_OK ) &&(ucArray_ID[0]!=admin_card[0])&&(ucArray_ID[1]!=admin_card[1])&&(ucArray_ID[2]!=admin_card[2])&&(ucArray_ID[3]!=admin_card[3])){
												sprintf ( cStr, "Add Card ID is: %02X%02X%02X%02X", ucArray_ID [ 0 ], ucArray_ID [ 1 ], ucArray_ID [ 2 ], ucArray_ID [ 3 ] );							
												printf ( "%s\r\n",cStr );
												WriteUser(); //写入用户
												flag = 0;	//状态为置0退出添加模式
												printf("Save Successfull\r\n");
											}
										}
							}	
					}
					//非管理卡检索Flash执行开门动作
					CheckStatus();
			}	
		}
		ReadUser();
//		Start_TCP_Client();
	}

} 



